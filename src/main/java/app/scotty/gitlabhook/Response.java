package app.scotty.gitlabhook;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class Response {

    private String message;
}