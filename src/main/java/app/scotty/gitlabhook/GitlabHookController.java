package app.scotty.gitlabhook;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
class GitlabHookController {

    private final RestTemplate restTemplate;

    public GitlabHookController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostMapping("/hook")
    public ResponseEntity<Response> mergeRequestHook(@RequestParam String projectId, @RequestParam String token, @RequestBody HookRequest hookRequest) {
        System.out.println("Hook Request Received!");
        Object branchName = hookRequest.getObject_attributes().get("source_branch");

        String uri = "https://gitlab.com/api/v4/projects/{projectId}/ref/{branchName}/trigger/pipeline?token={token}";
        restTemplate.exchange(uri, HttpMethod.POST, null, Void.class, projectId, branchName, token);

        return ResponseEntity
                .ok()
                .body(Response.builder()
                        .message("Pipeline Triggered!")
                        .build());
    }
}