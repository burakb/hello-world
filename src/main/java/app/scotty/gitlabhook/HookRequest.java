package app.scotty.gitlabhook;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * Example request:
 *
 * {
 *   "object_kind": "merge_request",
 *   "event_type": "merge_request",
 *   "user": {
 *     "name": "Burak BALDIRLIOĞLU",
 *     "username": "burakb",
 *     "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/707751/avatar.png"
 *   },
 *   "project": {
 *     "id": 8909085,
 *     "name": "hello-world",
 *     "description": "hello-world for GKE",
 *     "web_url": "https://gitlab.com/burakb/hello-world",
 *     "avatar_url": null,
 *     "git_ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *     "git_http_url": "https://gitlab.com/burakb/hello-world.git",
 *     "namespace": "burakb",
 *     "visibility_level": 20,
 *     "path_with_namespace": "burakb/hello-world",
 *     "default_branch": "master",
 *     "ci_config_path": null,
 *     "homepage": "https://gitlab.com/burakb/hello-world",
 *     "url": "git@gitlab.com:burakb/hello-world.git",
 *     "ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *     "http_url": "https://gitlab.com/burakb/hello-world.git"
 *   },
 *   "object_attributes": {
 *     "assignee_id": null,
 *     "author_id": 707751,
 *     "created_at": "2018-10-17 22:20:50 UTC",
 *     "description": "",
 *     "head_pipeline_id": 33350224,
 *     "id": 17914081,
 *     "iid": 1,
 *     "last_edited_at": null,
 *     "last_edited_by_id": null,
 *     "merge_commit_sha": "758bd6af10a44ab569156f4cf487e0113981991d",
 *     "merge_error": null,
 *     "merge_params": {
 *       "force_remove_source_branch": "1"
 *     },
 *     "merge_status": "can_be_merged",
 *     "merge_user_id": null,
 *     "merge_when_pipeline_succeeds": false,
 *     "milestone_id": null,
 *     "source_branch": "gitlab-ci-improvement",
 *     "source_project_id": 8909085,
 *     "state": "merged",
 *     "target_branch": "master",
 *     "target_project_id": 8909085,
 *     "time_estimate": 0,
 *     "title": "some improvements on gitlab ci file",
 *     "updated_at": "2018-10-17 22:37:34 UTC",
 *     "updated_by_id": 707751,
 *     "url": "https://gitlab.com/burakb/hello-world/merge_requests/1",
 *     "source": {
 *       "id": 8909085,
 *       "name": "hello-world",
 *       "description": "hello-world for GKE",
 *       "web_url": "https://gitlab.com/burakb/hello-world",
 *       "avatar_url": null,
 *       "git_ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *       "git_http_url": "https://gitlab.com/burakb/hello-world.git",
 *       "namespace": "burakb",
 *       "visibility_level": 20,
 *       "path_with_namespace": "burakb/hello-world",
 *       "default_branch": "master",
 *       "ci_config_path": null,
 *       "homepage": "https://gitlab.com/burakb/hello-world",
 *       "url": "git@gitlab.com:burakb/hello-world.git",
 *       "ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *       "http_url": "https://gitlab.com/burakb/hello-world.git"
 *     },
 *     "target": {
 *       "id": 8909085,
 *       "name": "hello-world",
 *       "description": "hello-world for GKE",
 *       "web_url": "https://gitlab.com/burakb/hello-world",
 *       "avatar_url": null,
 *       "git_ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *       "git_http_url": "https://gitlab.com/burakb/hello-world.git",
 *       "namespace": "burakb",
 *       "visibility_level": 20,
 *       "path_with_namespace": "burakb/hello-world",
 *       "default_branch": "master",
 *       "ci_config_path": null,
 *       "homepage": "https://gitlab.com/burakb/hello-world",
 *       "url": "git@gitlab.com:burakb/hello-world.git",
 *       "ssh_url": "git@gitlab.com:burakb/hello-world.git",
 *       "http_url": "https://gitlab.com/burakb/hello-world.git"
 *     },
 *     "last_commit": {
 *       "id": "4efc97e98f6f9e75b13a4b47f9edbcb9d078cd53",
 *       "message": "some improvements on gitlab ci file\n",
 *       "timestamp": "2018-10-17T22:34:27Z",
 *       "url": "https://gitlab.com/burakb/hello-world/commit/4efc97e98f6f9e75b13a4b47f9edbcb9d078cd53",
 *       "author": {
 *         "name": "Burak Baldırlıoğlu",
 *         "email": "burak.baldirlioglu@craftbase.io"
 *       }
 *     },
 *     "work_in_progress": false,
 *     "total_time_spent": 0,
 *     "human_total_time_spent": null,
 *     "human_time_estimate": null
 *   },
 *   "labels": [
 *
 *   ],
 *   "changes": {
 *     "total_time_spent": {
 *       "previous": null,
 *       "current": 0
 *     }
 *   },
 *   "repository": {
 *     "name": "hello-world",
 *     "url": "git@gitlab.com:burakb/hello-world.git",
 *     "description": "hello-world for GKE",
 *     "homepage": "https://gitlab.com/burakb/hello-world"
 *   }
 * }
 */
@Getter
@Setter
class HookRequest {

    private String event_type;
    private Map<String, Object> object_attributes;
}
