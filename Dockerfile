FROM openjdk:10-jre-slim-sid

COPY target/gitlab-hook-0.0.1-SNAPSHOT.jar lib/gitlab-hook.jar

ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS -jar lib/gitlab-hook.jar