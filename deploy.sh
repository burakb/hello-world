#!/usr/bin/env bash

apt-get update \
  && apt-get -y install gettext-base \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ARG=${1}
export CONTAINER_IMAGE=${ARG}
for f in ./kubernetes/tmpl/*.yml
do
  envsubst < $f > "./kubernetes/$(basename $f)"
done

kubectl apply -f ./kubernetes/